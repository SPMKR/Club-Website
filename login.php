<?php

require_once "mysqlHelper.php";
require_once "authenticate.php";

require_once __DIR__ .'/libraries/csrf/csrfprotector.php';
csrfProtector::init();

use \Firebase\JWT\JWT;

// If the user is already logged in, redirect
if ($authenticated) {
    header('Location: '.'members/index.php');
    exit();
}

// NOTE: All of this code below is for logging in logic

// Define vars and make sure they're empty
$studentID = $password = "";
$captchaValid = true; //FIXME: Turn this to false in production! This is disabled for local testing purposes only
$isLoggingIn = false;

// This will contain all the errors during login
$loginErrors = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Valid POST command. Attempt to login

    # Verify captcha
    $post_data = http_build_query(
        array(
            'secret' => '6LdbjCoUAAAAAAreeT9Yy7o_-qKsZodKOhJq6LmL',
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        )
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);
    if (!($captchaValid || $result->success)) {
        $loginErrors .= "<p>CAPTCHA failed. Please retry the CAPTCHA challenge</p>";
    } else {
        $captchaValid = true;
    }

    $studentID = strtolower(filter_var($_POST["studentID"], FILTER_SANITIZE_STRING)); // This one is alphanumeric only
    $password = filter_var($_POST["password"], FILTER_SANITIZE_FULL_SPECIAL_CHARS); // Allow special characters

    $loginQuery = "SELECT password FROM members WHERE username = ?";
    $loginStatement = mysqli_prepare($link, $loginQuery);
    if ($loginStatement && $captchaValid) {
        mysqli_stmt_bind_param($loginStatement, "s", $studentID);
        mysqli_stmt_execute($loginStatement);
        $loginError = mysqli_stmt_error($loginStatement);
        if (strlen($loginErrors) > 0) {
            $loginErrors .= "<p>$loginError</p>";
        } else {
            // No errors! Check if the login returns anything
            $results = mysqli_fetch_assoc_all(mysqli_stmt_get_result($loginStatement));
            if (count($results) > 0 && count($results) < 2) {
                // User exists, verify password
                if (password_verify($password, $results[0]["password"])) {
                    // Password is valid! Give user JWT and redirect to members/index.php
                    $token = array(
                        "nbf" => time(),
                        "exp" => time() + 60*60, //1 hour expiry
                        "aud" => $studentID
                    );
                    $loginToken = JWT::encode($token, "X#XGlc\$O]D4n@l)&t-l,Q^>/=%41-\"6Wz`8!.#cpGMjQckl6<\$AE6vJ[8X#G!'7X");
                    setcookie("authorization", $loginToken, time() + 60*60, "/", "", false, true); //1 hour expiry
                    header('Location: '.'members/index.php');
                    exit();
                } else {
                    $loginErrors .= "<p>Your username or password is wrong</p>";
                }
            } else {
                $loginErrors .= "<p>Your username or password is wrong</p>";
            }
        }
        mysqli_stmt_close($loginStatement);
    } else {
        // Unable to prepare statement for login
        if (!$loginStatement) {
            $loginErrors .= "<p>Login failed, error code: FAILED_PREP_SQL</p>";
        }
    }
//    $members_result = mysqli_query($link, );
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Log In | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="assets/css/main.css"/>

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong><a href="index.php">Make</a></strong> by SP</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="index.php#about">About Us</a></li>
            <li><a href="index.php#portfolio">Portfolio</a></li>
            <li><a href="index.php#exco">Our Exco</a></li>
            <li><a href="index.php#contactUs">Contact Us</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="#">Members' Area</a></li>
        </ul>
    </nav>
</header>

<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">

<!--        <header class="major special">-->
<!--            <h2>Login</h2>-->
<!--            <p>Lorem ipsum dolor sit amet nullam id egestas urna aliquam</p>-->
<!--        </header>-->

        <!-- Form -->
        <section>
            <h3>Log In</h3>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="row uniform 50%">
                    <div class="6u 12u$(xsmall)">
                        <input type="text" name="studentID" value="" placeholder="Student ID (pXXXXXXX)"/>
                    </div>
                </div>
                <div class="row uniform 50%">
                    <div class="6u 12u$(xsmall)">
                        <input type="password" name="password" value="" placeholder="Password"/>
                    </div>
                    <div class="12u$">
                        <div class="g-recaptcha" data-sitekey="6LdbjCoUAAAAAGo3X_UdZKX9OGj19KB6iQd45qI4"></div>
                    </div>
                    <div class="12u$">
                        <ul class="actions">
                            <li><input type="submit" value="Log In" class="special"/></li>
                            <li><input type="button" onclick="location.href='register.php'" value="Register"/></li>
                        </ul>
                    </div>
                </div>
            </form>
            <?php echo $loginErrors; ?>
        </section>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
            <!--            <li><a href="#" class="icon fa-twitter"></a></li>-->
            <!--            <li><a href="#" class="icon fa-instagram"></a></li>-->
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

</body>
</html>