<?php

require_once "mysqlHelper.php";

require_once __DIR__ .'/libraries/csrf/csrfprotector.php';
csrfProtector::init();

$searchBarContents = "";

if (isset($_POST["search"])) {
    // User is searching for something
    $searchBarContents = htmlspecialchars($_POST["search"]);
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>News | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong><a href="index.php">Make</a></strong> by SP</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="index.php#about">About Us</a></li>
            <li><a href="index.php#portfolio">Portfolio</a></li>
            <li><a href="index.php#exco">Our Exco</a></li>
            <li><a href="contact_us.php">Contact Us</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="members/index.php">Members' Area</a></li>
        </ul>
    </nav>
</header>

<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">
        <!-- Search Bar -->
        <section>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="row uniform 50%">
                    <div class="11u 12u$(xsmall)">
                        <input type="text" name="search" id="name" value="<?php echo $searchBarContents; ?>" placeholder="Search for an article title"/>
                    </div>
                    <div class="1u 12u$(xsmall)">
                        <ul class="actions">
                            <li><input type="submit" value="Search" class="special small"/></li>
                        </ul>
                    </div>
                </div>
            </form>
        </section>
        <?php
        if (strlen($searchBarContents) > 0) {
            // Perform Search
            $searchQuery = "SELECT * FROM news_articles WHERE newsTitle LIKE ?";
            $searchStatement = mysqli_prepare($link, $searchQuery);
            if ($searchStatement) {
                $searchParameter = "%{$_POST['search']}%";
                mysqli_stmt_bind_param($searchStatement, "s", $searchParameter);
                mysqli_stmt_execute($searchStatement);
                $searchError = mysqli_stmt_error($searchStatement);
                if (strlen($searchError) > 0) {
                    // Error occurred
                    echo "
        <header class=\"major special\">
            <h2>An error occurred when searching</h2>
            <p>We are currently working on fixing the server as fast as we can</p>
        </header>";
                } else {
                    $searchRows = mysqli_fetch_assoc_all(mysqli_stmt_get_result($searchStatement));
                    if (count($searchRows) < 1) {
                        // No search results
                        echo "
        <header class=\"major special\">
            <h2>No results</h2>
            <p>No title matches your search</p>
        </header>";
                    } else {
                        // Returned some search results
                        require_once "libraries/Parsedown.php";
                        $parseDown = new Parsedown();

                        for ($i = 0; $i < count($searchRows); $i++) {
                            echo
        "<header class=\"major special\">
            <h2>".$searchRows[$i]['newsTitle']."</h2>
            <p>Posted by ".$searchRows[$i]['author']."</p>
        </header>";

                            $parsedText = $parseDown->text($searchRows[$i]['newsDetails']);
                            echo $parsedText;

                            if ($i < count($searchRows) - 1) {
                                // It's not the last row yet
                                echo "<hr />";
                            }
                        }
                    }
                }
                mysqli_stmt_close($searchStatement);
            } //TODO: Add error handling
        } else {
            // Just list
            $news_result = mysqli_query($link, "SELECT * FROM news_articles ORDER BY postedDate DESC");
            $news_count = mysqli_num_rows($news_result);

            if ($news_count < 1) {
                // No articles
                echo "
        <header class=\"major special\">
            <h2>No articles</h2>
            <p>We currently do not have any news articles</p>
        </header>";
            } else {
                // There are articles!
                require_once "libraries/Parsedown.php";
                $parseDown = new Parsedown();

                $news_rows = mysqli_fetch_assoc_all($news_result);
                for ($i = 0; $i < $news_count; $i++) {
                    echo
        "<div class='row uniform 50%'>
            <header class=\"major special\">
                <h2>".htmlspecialchars($news_rows[$i]['newsTitle'])."</h2>
                <p>Posted by ".htmlspecialchars($news_rows[$i]['author'])." on ".htmlspecialchars(date("F j, Y, g:i a", strtotime($news_rows[$i]["postedDate"])))."</p>
            </header>";

                    $parsedText = $parseDown->text($news_rows[$i]['newsDetails']);
                    echo $parsedText;
                    echo "</div>";

                    if ($i < $news_count - 1) {
                        // It's not the last row yet
                        echo "<hr />";
                    }
                }
            }
        }
        mysqli_close($link);
        ?>
    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>