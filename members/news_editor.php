<?php

require '../mysqlHelper.php';

require_once __DIR__ .'/../libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (!$authenticated) {
    // If not authenticated, redirect to login
    header('Location: '.'../login.php');
    exit();
}

$newsSubmissionMessage = ""; // This will be displayed at the end of the form

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $newsTitle = $newsDetails = "";

    $newsTitle = $_POST["newsTitle"];
    $newsDetails = $_POST["newsDetails"];

    // Validation of length
    if (strlen($newsTitle) > 127) {
        $newsSubmissionMessage .= "<p>Title is too long, it needs to be limited to 127 characters only.</p>";
    }
    if (strlen($newsDetails) > 1023) {
        $newsSubmissionMessage .= "<p>Article is too long, it needs to be limited to 1023 characters only.</p>";
    }

    // Final check to submit only when all fields are valid
    if (strlen($newsSubmissionMessage) < 1) {
        $newsTitle = substr(filter_var($newsTitle, FILTER_SANITIZE_FULL_SPECIAL_CHARS), 0, 127);
        $newsDetails = substr(filter_var($newsDetails, FILTER_SANITIZE_FULL_SPECIAL_CHARS), 0, 1023);

        $newsEditQuery = "INSERT INTO contact_us (name, email, comments) VALUES (?, ?, ?)";
        $newsEditStatement = mysqli_prepare($link, $newsEditQuery);
        if ($newsEditStatement) {
            // DBMS successfully accepted prepared statement
            mysqli_stmt_bind_param($newsEditStatement, "sss", $name, "", ""); //this is actually correct, ignore the warning
            mysqli_stmt_execute($newsEditStatement);
            $newsEditStatus = mysqli_stmt_error($newsEditStatement);
            if (strlen($newsEditStatus) == 0) {
                // Successfully registered!!!
                $newsSubmissionMessage = "<p>Successfully posted article!</p>";
            } else {
                echo "Something wrong";
            }
            mysqli_stmt_close($newsEditStatement);
        } else {
            $newsSubmissionMessage .= "<p>Registration failed, error code: FAILED_PREP_SQL</p>";
        }
    }
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Edit Article | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="../assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong><a href="index.php">Make</a></strong> by SP</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Forums</a></li>
            <?php if ($isAdministrator) { echo "<li><a href=\"#\">Management Panel</a></li>"; } ?>
            <li><a href="?logout">Logout</a></li>
        </ul>
    </nav>
</header>

<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">
        <section>
            <h2>News Editor</h2>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="row uniform 50%">
                    <div class="12u$">
                        <input type="text" name="newsTitle" id="name" value="" placeholder="Title of article"/>
                    </div>
                    <div class="12u$">
                        <textarea name="newsDetails" id="message" placeholder="News contents here, uses Markdown" rows="10"></textarea>
                    </div>
                    <div class="12u$">
                        <ul class="actions">
                            <li><input type="submit" value="Update Article" class="special"/></li>
                            <li><input type="reset" value="Reset"/></li>
                        </ul>
                    </div>
                </div>
            </form>
            <?php echo $newsSubmissionMessage; ?>
        </section>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/skel.min.js"></script>
<script src="../assets/js/util.js"></script>
<script src="../assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>