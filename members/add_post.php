<?php

require_once "../authenticate.php";

require_once __DIR__ .'/../libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (!$authenticated) {
    // If not authenticated, redirect to login
    header('Location: '.'../login.php');
    exit();
}

// Allow user to log out
if (isset($_GET["logout"])) {
    // Log the user out
    setcookie("authorization", "", time() - 60*60, "/", "", false, true);
    header('Location: '.'../index.php');
    exit();
}

require_once "../mysqlHelper.php";

// Set variables
$realName = "";
$isAdministrator = false;

// Get name of member and check if user is admin
$userStatusQuery = "SELECT * FROM members WHERE username = ?";
$userStatusStatement = mysqli_prepare($link, $userStatusQuery);
if ($userStatusStatement) {
    mysqli_stmt_bind_param($userStatusStatement, "s", $userID);
    mysqli_stmt_execute($userStatusStatement);
//    $userStatusError = mysqli_stmt_error($userStatusStatement);
    $userStatusResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($userStatusStatement));
    if (count($userStatusResults) > 0 && count($userStatusResults) < 2) {
        $realName = htmlspecialchars(filter_var($userStatusResults[0]["name"], FILTER_SANITIZE_STRING));
        $isAdministrator = $userStatusResults[0]["administrator"];
    } else {
        // It has encountered an error and needs to quit
    }
    mysqli_stmt_close($userStatusStatement);
}

/////////////// All logic below is for adding forum posts ///////////////
$postSubmissionMessage = $category = '';
$categoryIDFiltered = -1;

if (isset($_POST['postContent'])) {
    // The user is trying to submit a new post. This function must redirect, since the GET function below will produce an error

    $categoryIDFiltered = filter_var($_POST['category'], FILTER_SANITIZE_NUMBER_INT);

    // Check if category is valid in the first place
    $categoryExists = false;
    $categoryQuery = "SELECT * FROM post_categories WHERE categoryID = ?";
    $categoryStatement = mysqli_prepare($link, $categoryQuery);
    if ($categoryStatement) {
        mysqli_stmt_bind_param($categoryStatement, 'i', $categoryIDFiltered);
        mysqli_stmt_execute($categoryStatement);
        $categoryError = mysqli_stmt_error($categoryStatement);
        $categoryResult = mysqli_fetch_assoc_all(mysqli_stmt_get_result($categoryStatement));
        if (strlen($categoryError) < 1 && count($categoryResult) == 1) {
            // All good
            $categoryExists = true;
        }
    }

    if ($categoryExists) {
        // Continue posting
        $title = $_POST['title'];
        $postContent = $_POST['postContent'];

        //TODO: Implement length checking here

        $postQuery = "INSERT INTO posts (author, category, postTitle, postDetail) VALUES (?, ?, ?, ?)";
        $postStatement = mysqli_prepare($link, $postQuery);
        if ($postStatement) {
            // DBMS successfully accepted prepared statement
            mysqli_stmt_bind_param($postStatement, "siss", $realName, $categoryIDFiltered, $title, $postContent);
            mysqli_stmt_execute($postStatement);
            $postError = mysqli_stmt_error($postStatement);
            if (strlen($postError) == 0) {
                // Successfully posted
                $category = urlencode($categoryIDFiltered);
                header("Location: forum.php?section=$category");
                exit();
            }
            mysqli_stmt_close($postStatement);
        }
    } else {
        header("Location: discussion.php");
        exit();
    }
}

$categoryName = "";

// Get category name and check if it's valid
if (isset($_GET['category'])) {
    $categoryIDFiltered = filter_var($_GET['category'], FILTER_SANITIZE_NUMBER_INT);

    // Check if category exists in DB
    $categoryExists = false;
    $categoryQuery = "SELECT * FROM post_categories WHERE categoryID = ?";
    $categoryStatement = mysqli_prepare($link, $categoryQuery);
    if ($categoryStatement) {
        mysqli_stmt_bind_param($categoryStatement, 'i', $categoryIDFiltered);
        mysqli_stmt_execute($categoryStatement);
        $categoryError = mysqli_stmt_error($categoryStatement);
        $categoryResult = mysqli_fetch_assoc_all(mysqli_stmt_get_result($categoryStatement));
        if (strlen($categoryError) < 1 && count($categoryResult) == 1) {
            // All good
            $categoryName = $categoryResult[0]['categoryName'];
            $categoryDescription = $categoryResult[0]['categoryDescription'];
            $categoryExists = true;
        }
    }

    if (!$categoryExists) {
        header("Location: discussion.php");
        exit();
    }


//    $categoriesResult = mysqli_query($link, "SHOW COLUMNS FROM posts WHERE Field = 'category'");
//    preg_match("/^enum\(\'(.*)\'\)$/", mysqli_fetch_assoc_all($categoriesResult)[0]["Type"], $matches);
//    $categories = explode("','", $matches[1]);
//
//    $categoryExists = false;
//    foreach ($categories as $cat) {
//        if ($cat == $_GET['category']) {
//            $categoryExists = true;
//        }
//    }
//
//    if (!$categoryExists) {
//        header("Location: discussion.php");
//        exit();
//    }
} else {
    header("Location: discussion.php");
    exit();
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Add Forum Post | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="../assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong>Members</strong>Centre</h1>
    <nav id="nav">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="discussion.php">Forums</a></li>
            <?php
            if ($isAdministrator) {
                echo "<li><a href='#'>Admin Panel</a></li>";
            } ?>
            <li><a href="management.php">My Account</a></li>
            <li><a href="?logout">Logout</a></li>
        </ul>
    </nav>
</header>

<a href="#" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">

        <section>
            <h2>Add Post <?php echo "To ".$categoryName ?></h2>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="row uniform 50%">
                    <div class="12u$">
                        <input type="text" name="title" id="name" value="" placeholder="Title"/>
                    </div>
                    <div class="12u$">
                        <textarea name="postContent" id="message" placeholder="Post Content (max 1023 characters, accepts Markdown formatting)" rows="6"></textarea>
                    </div>
                    <input type="hidden" name="category" value="<?php echo htmlspecialchars($_GET['category']) ?>">
                    <div class="12u$">
                        <ul class="actions">
                            <li><input type="submit" value="Submit Post" class="special"/></li>
                            <li><input type="reset" value="Reset"/></li>
                        </ul>
                    </div>
                </div>
            </form>
            <?php echo $postSubmissionMessage; ?>
        </section>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/skel.min.js"></script>
<script src="../assets/js/util.js"></script>
<script src="../assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>