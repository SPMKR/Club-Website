<?php

require_once "../authenticate.php";

require_once __DIR__ .'/../libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (!$authenticated) {
    // If not authenticated, redirect to login
    header('Location: '.'../login.php');
    exit();
}

// Allow user to log out
if (isset($_GET["logout"])) {
    // Log the user out
    setcookie("authorization", "", time() - 60*60, "/", "", false, true);
    header('Location: '.'../index.php');
    exit();
}

require_once "../mysqlHelper.php";

// Set variables
$realName = "";
$isAdministrator = false;

// Get name of member and check if user is admin
$userStatusQuery = "SELECT * FROM members WHERE username = ?";
$userStatusStatement = mysqli_prepare($link, $userStatusQuery);
if ($userStatusStatement) {
    mysqli_stmt_bind_param($userStatusStatement, "s", $userID);
    mysqli_stmt_execute($userStatusStatement);
//    $userStatusError = mysqli_stmt_error($userStatusStatement);
    $userStatusResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($userStatusStatement));
    if (count($userStatusResults) > 0 && count($userStatusResults) < 2) {
        $realName = htmlspecialchars(filter_var($userStatusResults[0]["name"], FILTER_SANITIZE_STRING));
        $isAdministrator = $userStatusResults[0]["administrator"];
    } else {
        // It has encountered an error and needs to quit
    }
    mysqli_stmt_close($userStatusStatement);
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Member's Area | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="../assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong>Members</strong>Centre</h1>
    <nav id="nav">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="discussion.php">Forums</a></li>
            <?php if ($isAdministrator) { echo "<li><a href='admin/edit_exco.php'>Admin Panel</a></li>"; } ?>
            <li><a href="management.php">My Account</a></li>
            <li><a href="?logout">Logout</a></li>
        </ul>
    </nav>
</header>

<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">

        <header class="major special">
            <h2>Welcome, <?php echo $realName; ?></h2>
            <p>You're logged in at <?php echo date("F j, Y, g:i a", $issuedAt) ?>, your session expires at <?php echo date("F j, Y, g:i a", $expiresAt) ?> and your session is secured by HMAC-SHA256. Enjoy your stay</p>
        </header>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/skel.min.js"></script>
<script src="../assets/js/util.js"></script>
<script src="../assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>