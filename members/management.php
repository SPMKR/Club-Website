<?php

require_once "../authenticate.php";

require_once __DIR__ .'/../libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (!$authenticated) {
    // If not authenticated, redirect to login
    header('Location: '.'../login.php');
    exit();
}

// Allow user to log out
if (isset($_GET["logout"])) {
    // Log the user out
    setcookie("authorization", "", time() - 60*60, "/", "", false, true);
    header('Location: '.'../index.php');
    exit();
}

require_once "../mysqlHelper.php";

// Set variables
$studentID = $realName = $oldPassword = "";
$isAdministrator = false;

// Get name of member and check if user is admin
$userStatusQuery = "SELECT * FROM members WHERE username = ?";
$userStatusStatement = mysqli_prepare($link, $userStatusQuery);
if ($userStatusStatement) {
    mysqli_stmt_bind_param($userStatusStatement, "s", $userID);
    mysqli_stmt_execute($userStatusStatement);
//    $userStatusError = mysqli_stmt_error($userStatusStatement);
    $userStatusResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($userStatusStatement));
    if (count($userStatusResults) > 0 && count($userStatusResults) < 2) {
        $studentID = htmlspecialchars(filter_var($userStatusResults[0]["username"], FILTER_SANITIZE_STRING));
        $realName = htmlspecialchars(filter_var($userStatusResults[0]["name"], FILTER_SANITIZE_STRING));
        $oldPassword = $userStatusResults[0]["password"];
        $isAdministrator = $userStatusResults[0]["administrator"];
    } else {
        // It has encountered an error and needs to quit
    }
    mysqli_stmt_close($userStatusStatement);
}

/* Everything below this line is code responsible for changing password */

// Returns empty string if no password violations, otherwise, returns all password violations
function validatePassword($password) {
    $passwordViolations = "";
    if (strlen($password) < 10) {
        // Password is less than 10 characters
        $passwordViolations .= "<p>Password cannot be shorter than 10 characters</p>";
    }
    if (!preg_match("#[0-9]+#",$password)) {
        $passwordViolations .= "<p>Password must contain at least 1 number</p>";
    }
    if (!preg_match("#[A-Z]+#",$password)) {
        $passwordViolations .= "<p>Password must contain at least 1 upper case letter</p>";
    }
    if(!preg_match("#[a-z]+#",$password)) {
        $passwordViolations .= "<p>Password must contain at least 1 lower case letter</p>";
    }
    return $passwordViolations;
}

// Set variables
$passwordChangeStatus = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Valid POST command. Attempt to change password

    $newPassword = $_POST['newPassword'];
    $newPasswordAgain = $_POST['newPasswordAgain'];

    // Validate old password
    if (!password_verify($_POST['oldPassword'], $oldPassword)) {
        $passwordChangeStatus .= "<p>Old password is invalid</p>";
    }

    // Check matching password
    if (!($newPassword == $newPasswordAgain)) {
        $passwordChangeStatus .= "<p>Passwords do not match</p>";
    }

    $passwordChangeStatus .= validatePassword($newPassword);

    if (strlen($passwordChangeStatus) < 1) {
        // Go for password change
        $updatePasswordQuery = "UPDATE members SET password = ? WHERE username = ?";
        $updatePasswordStatement = mysqli_prepare($link, $updatePasswordQuery);
        if ($updatePasswordStatement) {
            mysqli_stmt_bind_param($updatePasswordStatement, "ss", password_hash($newPassword, PASSWORD_DEFAULT), $userID);
            mysqli_stmt_execute($updatePasswordStatement);
            if (mysqli_stmt_affected_rows($updatePasswordStatement) > 0) {
                // Success!
                $passwordChangeStatus .= "<p>Successfully changed password!</p>";
            } else {
                // Failed to update password
                $passwordChangeStatus .= "<p>Change password failed, error code: FAILED_SQL_QUERY</p>";
            }
        } else {
            $passwordChangeStatus .= "<p>Change password failed, error code: FAILED_PREP_SQL</p>";
        }
    }
}

?>

<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>My Account | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="../assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong>Members</strong>Centre</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="discussion.php">Forums</a></li>
            <?php if ($isAdministrator) { echo "<li><a href='#'>Admin Panel</a></li>"; } ?>
            <li><a href="#">My Account</a></li>
            <li><a href="?logout">Logout</a></li>
        </ul>
    </nav>
</header>

<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">
        <!-- Form -->
        <section>
            <h3>My Account</h3>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="row uniform 50%">
                    <div class="6u$ 12u$(xsmall)">
                        <input type="text" placeholder="Student ID" value="<?php echo $studentID ?>" disabled="disabled"/>
                    </div>
                    <div class="6u$ 12u$(xsmall)">
                        <input type="text" placeholder="Real Name" value="<?php echo $realName ?>" disabled="disabled"/>
                    </div>
                    <div class="12u$">
                        <h4>Change Password</h4>
                    </div>
                    <div class="6u$ 12u$(xsmall)">
                        <input type="password" name="oldPassword" value="" placeholder="Old Password"/>
                    </div>
                    <div class="6u$ 12u$(xsmall)">
                        <input type="password" name="newPassword" value="" placeholder="New Password"/>
                    </div>
                    <div class="6u$ 12u$(xsmall)">
                        <input type="password" name="newPasswordAgain" value="" placeholder="New Password Again"/>
                    </div>
                </div>
                <div class="row uniform 50%">
                    <div class="12u$">
                        <ul class="actions">
                            <li><input type="submit" value="Change Password" class="special"/></li>
                            <li><input type="reset" value="Reset"/></li>
                        </ul>
                    </div>
                    <?php echo $passwordChangeStatus ?>
                </div>
            </form>
        </section>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/skel.min.js"></script>
<script src="../assets/js/util.js"></script>
<script src="../assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>