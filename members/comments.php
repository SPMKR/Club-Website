<?php

require_once "../authenticate.php";

require_once __DIR__ .'/../libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (!$authenticated) {
    // If not authenticated, redirect to login
    header('Location: '.'../login.php');
    exit();
}

require_once "../mysqlHelper.php";

// Allow user to log out
if (isset($_GET["logout"])) {
    // Log the user out
    setcookie("authorization", "", time() - 60*60, "/", "", false, true);
    header('Location: '.'../index.php');
    exit();
}

// check if user is admin
$isAdministrator = false;
$userStatusQuery = "SELECT administrator FROM members WHERE username = ?";
$userStatusStatement = mysqli_prepare($link, $userStatusQuery);
if ($userStatusStatement) {
    mysqli_stmt_bind_param($userStatusStatement, "s", $userID);
    mysqli_stmt_execute($userStatusStatement);
//    $userStatusError = mysqli_stmt_error($userStatusStatement);
    $userStatusResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($userStatusStatement));
    if (count($userStatusResults) > 0 && count($userStatusResults) < 2) {
        $isAdministrator = $userStatusResults[0]["administrator"];
    } else {
        // It has encountered an error and needs to quit
    }
    mysqli_stmt_close($userStatusStatement);
}

$validRequest = false;

$postTitle = $postSubtitle = $postContents = $realName = "";
$postID = -1;

// Resolve user ID into real name
$userNameQuery = "SELECT name FROM members WHERE username = ?";
$userNameStatement = mysqli_prepare($link, $userNameQuery);
if ($userNameStatement) {
    mysqli_stmt_bind_param($userNameStatement, "s", $userID);
    mysqli_stmt_execute($userNameStatement);
    $nameResult = mysqli_fetch_assoc_all(mysqli_stmt_get_result($userNameStatement));
    if (count($nameResult) == 1) {
        $realName = $nameResult[0]["name"];
    } else {
        $commentStatus .= "Cannot resolve user ID";
    }
    mysqli_stmt_close($userNameStatement);
}

// Logic for comment deletion
if (isset($_GET["delete"])) {
    // Delete comment
    $targetCommentID = intval(filter_var($_GET["delete"], FILTER_SANITIZE_NUMBER_INT));
    // Get comment parent first
    $commentParentQuery = "SELECT * FROM comments WHERE commentID = ?";
    $commentParentStatement = mysqli_prepare($link, $commentParentQuery);
    $commentAuthor = "";
    if ($commentParentStatement) {
        mysqli_stmt_bind_param($commentParentStatement, "i", $targetCommentID);
        mysqli_stmt_execute($commentParentStatement);
        $commentParentResult = mysqli_fetch_assoc_all(mysqli_stmt_get_result($commentParentStatement));
        mysqli_stmt_close($commentParentStatement);

        if (count($commentParentResult) == 1) {
            // Valid
            $validRequest = true;
            $postID = $commentParentResult[0]["parent"];
            $commentAuthor = $commentParentResult[0]["author"];
        }
    }

    // If everything is valid, check if comment belongs to the current user
    if ($validRequest) {
        if ($commentAuthor == $realName) {
            $commentDeletionQuery = "DELETE FROM comments WHERE commentID = ?";
            $commentDeletionStatement = mysqli_prepare($link, $commentDeletionQuery);
            if ($commentDeletionStatement) {
                mysqli_stmt_bind_param($commentDeletionStatement, "i", $targetCommentID);
                mysqli_stmt_execute($commentDeletionStatement);
                if (mysqli_stmt_affected_rows($commentDeletionStatement) < 1) {
                    // Error
                    $commentStatus .= "<p>Error: Comment not deleted</p>";
                }
                mysqli_stmt_close($commentDeletionStatement);
            } else {
                $validRequest = false;
            }
        } else {
            $validRequest = false;
        }
    }
}

// Retrieve original post details
if (isset($_GET["postID"]) || $postID != -1) {
    // Valid request...sort of. Still need to validate if it's in database
    $postID = $postID != -1 ? $postID : filter_var($_GET["postID"], FILTER_SANITIZE_NUMBER_INT);

    // Check if post exists in DB and get title and details
    $postQuery = "SELECT * FROM posts WHERE postID = ?";
    $postStatement = mysqli_prepare($link, $postQuery);
    if ($postStatement) {
        mysqli_stmt_bind_param($postStatement, "s", $postID);
        mysqli_stmt_execute($postStatement);
        $postError = mysqli_stmt_error($postStatement);
        $postResult = mysqli_fetch_assoc_all(mysqli_stmt_get_result($postStatement));
        mysqli_stmt_close($postStatement);

        if (count($postResult) == 1) {
            $postID = $postResult[0]["postID"];
            $postTitle = htmlspecialchars($postResult[0]["postTitle"]);
            $postSubtitle = htmlspecialchars("Posted by ".$postResult[0]["author"]." at ".date("F j, Y, g:i a", strtotime($postResult[0]["postedDate"])));
            $postContents = $postResult[0]["postDetail"];
            $validRequest = true;
        } else {
            $validRequest = false;
        }
    }
} else {
    $validRequest = false;
}

// The following code is for comment submission only

$commentStatus = "";

if (isset($_POST["comment"])) {
    $validRequest = true;

    // Validate if parent post exists
    $postQuery = "SELECT * FROM posts WHERE postID = ?";
    $postStatement = mysqli_prepare($link, $postQuery);
    if ($postStatement) {
        mysqli_stmt_bind_param($postStatement, "s", $_POST["parent"]);
        mysqli_stmt_execute($postStatement);
        $postError = mysqli_stmt_error($postStatement);
        $postResult = mysqli_fetch_assoc_all(mysqli_stmt_get_result($postStatement));
        if (count($postResult) != 1) {
            $commentStatus .= "<p>Suspected CSRF attack. This incident will be reported.</p>";
        } else if (count($postResult) == 1) {
            $postTitle = htmlspecialchars($postResult[0]["postTitle"]);
            $postSubtitle = htmlspecialchars("Posted by ".$postResult[0]["author"]." at ".date("F j, Y, g:i a", strtotime($postResult[0]["postedDate"])));
            $postContents = $postResult[0]["postDetail"];
        }
        mysqli_stmt_close($postStatement);
    }

    if (strlen($_POST["comment"]) > 511) {
        // Too long
        $commentStatus .= "<p>Comment is too long, it must be less than 512 characters long</p>";
    }
    if (strlen($_POST["comment"]) < 1) {
        $commentStatus .= "<p>Comment is too short</p>";
    }

    if (strlen($commentStatus) == 0) {
        $postID = $_POST["parent"]; // Still show comments
        // Post comment
        $commentPostQuery = "INSERT INTO comments (parent, author, commentDetail) VALUES (?, ?, ?)";
        $commentPostStatement = mysqli_prepare($link, $commentPostQuery);
        if ($commentPostStatement) {
            mysqli_stmt_bind_param($commentPostStatement, "sss", $_POST["parent"], $realName, $_POST["comment"]);
            mysqli_stmt_execute($commentPostStatement);
            // Check affected rows
            if (mysqli_stmt_affected_rows($commentPostStatement) < 1) {
                $commentStatus .= "<p>Error: comment not submitted</p>";
            }
            mysqli_stmt_close($commentPostStatement);
        }
    }
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Comments | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="../assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong>Members</strong>Centre</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="discussion.php">Forums</a></li>
            <?php
            if ($isAdministrator) {
                echo "<li><a href='#'>Admin Panel</a></li>";
            } ?>
            <li><a href="management.php">My Account</a></li>
            <li><a href="?logout">Logout</a></li>
        </ul>
    </nav>
</header>

<a href="#" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">
        <?php
        require_once "../libraries/Parsedown.php";
        $parseDown = new Parsedown();

        // Populate news
        if ($validRequest) {
            echo
        "<header>
            <h3>$postTitle</h3>
            <p>$postSubtitle</p>
        </header>";
            echo $parseDown->text($postContents);
            echo "<hr /><h4>Comments</h4>";

            // Show comments here
            $commentsQuery = "SELECT * FROM comments WHERE parent = ? ORDER BY postedDate DESC";
            $commentsStatement = mysqli_prepare($link, $commentsQuery);
            if ($commentsStatement) {
                mysqli_stmt_bind_param($commentsStatement, "s", $postID);
                mysqli_stmt_execute($commentsStatement);
                $commentsError = mysqli_stmt_error($commentsStatement);
                $commentsResult = mysqli_fetch_assoc_all(mysqli_stmt_get_result($commentsStatement));
                mysqli_stmt_close($commentsStatement);

                if (count($commentsResult) > 0) {
                    // Load comments
                    for ($i = 0; $i < count($commentsResult); $i++) {
                        $commentAuthor = htmlspecialchars($commentsResult[$i]["author"]);
                        $commentContent = htmlspecialchars($commentsResult[$i]["commentDetail"]);
                        echo "<div class='row'>";
                        echo "<header><p>$commentAuthor</p></header>";
                        echo "<p>$commentContent</p>";
                        // Decide to show delete button or not. Only show if user == current user
                        if ($commentsResult[$i]["author"] == $realName) {
                            $commentID = urlencode($commentsResult[$i]["commentID"]);
                            echo "<a href=\"?delete=$commentID\" class=\"icon fa-times\"><span class=\"label\">Delete</span></a>";
                        }
                        echo "</div>";
                    }
                } else {
                    echo "<p>No comments</p>";
                }
            }

            // Show comment entry box here
            $self = htmlspecialchars($_SERVER["PHP_SELF"]);
            echo "
            <section>
            <form method=\"post\" action=\"$self\">
                <div class=\"row uniform 50%\">
                    <div class=\"12u$\">
                        <textarea name=\"comment\" id=\"message\" placeholder=\"Enter your comment here (max 512 characters)\" rows=\"3\"></textarea>
                        <input type=\"hidden\" name=\"parent\" value=\"".$postID."\">
                    </div>
                    <div class=\"12u$\">
                        <ul class=\"actions\">
                            <li><input type=\"submit\" value=\"Submit Comment\" class=\"special\"/></li>
                            <li><input type=\"reset\" value=\"Reset\"/></li>
                        </ul>
                    </div>
                </div>
            </form>
            $commentStatus
        </section>
            ";
        } else {
            echo
        "<header>
            <h3>Invalid request</h3>
        </header>";
        }
        ?>

<!--        <h4>Comments</h4>-->
<!--        <header>-->
<!--            <p>Pan Ziyue</p>-->
<!--        </header>-->
<!--        <p>This is a comment</p>-->
<!--        <header>-->
<!--            <p>Pan Ziyue</p>-->
<!--        </header>-->
<!--        <p>This is a comment</p>-->
    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/skel.min.js"></script>
<script src="../assets/js/util.js"></script>
<script src="../assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>