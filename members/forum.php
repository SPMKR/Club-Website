<?php

require_once "../authenticate.php";

require_once __DIR__ .'/../libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (!$authenticated) {
    // If not authenticated, redirect to login
    header('Location: '.'../login.php');
    exit();
}

require_once "../mysqlHelper.php";

$isAdministrator = false;

// Allow user to log out
if (isset($_GET["logout"])) {
    // Log the user out
    setcookie("authorization", "", time() - 60*60, "/", "", false, true);
    header('Location: '.'../index.php');
    exit();
}

// check if user is admin
$userStatusQuery = "SELECT administrator FROM members WHERE username = ?";
$userStatusStatement = mysqli_prepare($link, $userStatusQuery);
if ($userStatusStatement) {
    mysqli_stmt_bind_param($userStatusStatement, "s", $userID);
    mysqli_stmt_execute($userStatusStatement);
//    $userStatusError = mysqli_stmt_error($userStatusStatement);
    $userStatusResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($userStatusStatement));
    if (count($userStatusResults) > 0 && count($userStatusResults) < 2) {
        $isAdministrator = $userStatusResults[0]["administrator"];
    }
    mysqli_stmt_close($userStatusStatement);
}

$validRequest = false;
$categoryName = $categoryDescription = "";
$categoryIDFiltered = -1;

if (isset($_GET["section"])) {
    // Valid request...sort of. Still need to validate if it's in database

    $categoryIDFiltered = filter_var($_GET["section"], FILTER_SANITIZE_NUMBER_INT);

    // Check if category exists in DB
    $categoryExists = false;
    $categoryQuery = "SELECT * FROM post_categories WHERE categoryID = ?";
    $categoryStatement = mysqli_prepare($link, $categoryQuery);
    if ($categoryStatement) {
        mysqli_stmt_bind_param($categoryStatement, 'i', $categoryIDFiltered);
        mysqli_stmt_execute($categoryStatement);
        $categoryError = mysqli_stmt_error($categoryStatement);
        $categoryResult = mysqli_fetch_assoc_all(mysqli_stmt_get_result($categoryStatement));
        if (strlen($categoryError) < 1 && count($categoryResult) == 1) {
            // All good
            $categoryName = $categoryResult[0]['categoryName'];
            $categoryDescription = $categoryResult[0]['categoryDescription'];
            $categoryExists = true;
        }
    }

    if ($categoryExists) {
        // Valid request, resolve ID to category name
        $categoryName = htmlspecialchars($categoryName);
        $validRequest = true;
    }
} else {
    // Invalid request
    $validRequest = false;
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>
        <?php
        if ($validRequest) {
            echo $categoryName;
        } else {
            echo "Invalid Request";
        }
        ?> | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="../assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong>Members</strong>Centre</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="discussion.php">Forums</a></li>
            <?php
            if ($isAdministrator) {
                echo "<li><a href='#'>Admin Panel</a></li>";
            } ?>
            <li><a href="management.php">My Account</a></li>
            <li><a href="?logout">Logout</a></li>
        </ul>
    </nav>
</header>

<a href="#" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">

        <header class="major special">
            <div class="row">
                <div class="8u 12u$(xsmall)">
                    <?php
                    if (!$validRequest) {
                        echo "<h2>Invalid request</h2>
            <p>The request to this page is invalid</p>";
                    } else {
                        echo "<h2>$categoryName</h2>";
                        echo "<p>$categoryDescription</p>";
                    }
                    ?>
                </div>
                <?php
                $categoryName = urlencode(htmlspecialchars_decode($categoryName));
                if ($validRequest) {
                    echo "<br /><div class=\"4u 12u$(xsmall)\"><a href=\"add_post.php?category=$categoryIDFiltered\" class=\"button special fit\">Add Post</a></div>";
                } ?>
            </div>
        </header>

        <section>
            <?php
            $postsQuery = "SELECT * FROM posts WHERE category = ? ORDER BY postedDate DESC";
            $postsStatement = mysqli_prepare($link, $postsQuery);
            if ($postsStatement) {
                $sectionNameDecoded = htmlspecialchars_decode($categoryName);
                mysqli_stmt_bind_param($postsStatement, "i", $categoryIDFiltered);
                mysqli_stmt_execute($postsStatement);
                $postsResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($postsStatement));
                mysqli_stmt_close($postsStatement);

                if (count($postsResults) > 0) {
                    require_once "../libraries/Parsedown.php";
                    $parseDown = new Parsedown();

                    // Populate news
                    for ($i = 0; $i < count($postsResults); $i++) {
                        $title = htmlspecialchars($postsResults[$i]["postTitle"]);
                        $author = htmlspecialchars($postsResults[$i]["author"]);
                        $date = date("F j, Y, g:i a", strtotime($postsResults[$i]["postedDate"]));
                        echo
            "<header>
                <h3>$title</h3>
                <p>Posted by $author at $date</p>
            </header>";
                        echo $parseDown->text($postsResults[$i]['postDetail']);
                        $postID = urlencode($postsResults[$i]["postID"]);
                        // Print button
                        echo "<a href=\"comments.php?postID=$postID\" class=\"button alt fit small\">Comment</a>";
                        if ($i < count($postsResults) - 1) {
                            // Print horizontal rule
                            echo "<hr />";
                        }
                    }
                } else {
                    echo
            "<header>
                <h3>No posts</h3>
                <p>There are no posts in this category</p>
            </header>";
                }
            } else {
                // TODO: Output error here
            }
            ?>
        </section>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/skel.min.js"></script>
<script src="../assets/js/util.js"></script>
<script src="../assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>