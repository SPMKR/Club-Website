<?php

require_once "../../authenticate.php";

require_once __DIR__ . '/../../libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (!$authenticated) {
    // If not authenticated, redirect to login
    header('Location: '.'../../login.php');
    exit();
}

// Allow user to log out
if (isset($_GET["logout"])) {
    // Log the user out
    setcookie("authorization", "", time() - 60*60, "/", "", false, true);
    header('Location: '.'../../index.php');
    exit();
}

require_once "../../mysqlHelper.php";

// Set variables
$realName = "";
$isAdministrator = false;

// Run SQL stuff
$realNameQuery = "SELECT name FROM members WHERE username = ?";
$realNameStatement = mysqli_prepare($link, $realNameQuery);
if ($realNameStatement) {
    mysqli_stmt_bind_param($realNameStatement, "s", $userID);
    mysqli_stmt_execute($realNameStatement);
    $realNameError = mysqli_stmt_error($realNameStatement);
    $realNameResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($realNameStatement));
    if (count($realNameResults) > 0 && count($realNameResults) < 2) {
        $realName = $realNameResults[0]["name"];
        $realName = htmlspecialchars(filter_var($realName, FILTER_SANITIZE_STRING));
    }
}

// Check if user is admin
$adminQuery = "SELECT administrator FROM members WHERE username = ?";
$adminStatement = mysqli_prepare($link, $adminQuery);
if ($adminStatement) {
    mysqli_stmt_bind_param($adminStatement, "s", $userID);
    mysqli_stmt_execute($adminStatement);
    $adminError = mysqli_stmt_error($adminStatement);
    $adminResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($adminStatement));
    if (count($adminResults) > 0 && count($adminResults) < 2) {
        $isAdministrator = $adminResults[0]["administrator"];
    }
}

if (!$isAdministrator) {
    // Kick the user out!
    header('Location: '.'index.php');
    exit();
}

/* Everything here is image uploading logic */

$final_upload_state = ""; // this contains the final message to display to the user

if (isset($_FILES["fileToUpload"]["name"])) {
    $target_dir = "../../images/";

    $temp = explode(".", $_FILES["fileToUpload"]["name"]);
    $exco_name = $_POST["name"];
    $target_file = $target_dir . $exco_name . '.' . end($temp);

    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
//        echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
//        echo "File is not an image.";
            $uploadOk = 0;
        }
    }

    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        $final_upload_state = "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg") {
        $final_upload_state = "Only .jpg is allowed";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $final_upload_state = "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $final_upload_state = "The file has been uploaded.";
        } else {
            $final_upload_state = "Sorry, there was an error uploading your file.";
        }
    }
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Edit Exco | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="../../assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong>Admin</strong>Panel</h1>
    <nav id="nav">
        <ul>
            <li><a href="../index.php">Home</a></li>
            <li><a href="../discussion.php">Forums</a></li>
            <?php if ($isAdministrator) { echo "<li><a href=\"#\">Admin Panel</a></li>"; } ?>
            <li><a href="../management.php">My Account</a></li>
            <li><a href="?logout">Logout</a></li>
        </ul>
    </nav>
</header>

<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">

        <header class="major special">
            <h2>Edit Exco</h2>
            <p><?php echo $final_upload_state; ?></p>
        </header>

        <?php
        $exco_result = mysqli_query($link, "SELECT * FROM exco_members ORDER BY FIELD(role, 'President','Vice President','Secretary','Quartermaster')");
        while ($row = mysqli_fetch_assoc($exco_result)) {
            echo
        "<section>
            <h4>".$row['name']."</h4>
            <p><span class=\"image left\"><img src=\"../../images/".$row['name'].".jpg\" alt=\"".$row['name']."\"/></span>
                <br />
                <form action=\"".htmlspecialchars($_SERVER["PHP_SELF"])."\" method=\"post\" enctype=\"multipart/form-data\">
                    Select image to upload:
                    <input type=\"file\" name=\"fileToUpload\" class=\"special\">
                    <input type=\"submit\" value=\"Upload Image\" name=\"submit\">
                    <input type=\"hidden\" name=\"name\" value=\"".$row['name']."\">
                </form>
                <br />
                <br />
            </p>
        </section>";
        }
        ?>
    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="../../assets/js/jquery.min.js"></script>
<script src="../../assets/js/skel.min.js"></script>
<script src="../../assets/js/util.js"></script>
<script src="../../assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>