<?php

require_once "../authenticate.php";

require_once __DIR__ .'/../libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (!$authenticated) {
    // If not authenticated, redirect to login
    header('Location: '.'../login.php');
    exit();
}

require_once "../mysqlHelper.php";

// Allow user to log out
if (isset($_GET["logout"])) {
    // Log the user out
    setcookie("authorization", "", time() - 60*60, "/", "", false, true);
    header('Location: '.'../index.php');
    exit();
}

$isAdministrator = false;

// check if user is admin
$userStatusQuery = "SELECT administrator FROM members WHERE username = ?";
$userStatusStatement = mysqli_prepare($link, $userStatusQuery);
if ($userStatusStatement) {
    mysqli_stmt_bind_param($userStatusStatement, "s", $userID);
    mysqli_stmt_execute($userStatusStatement);
//    $userStatusError = mysqli_stmt_error($userStatusStatement);
    $userStatusResults = mysqli_fetch_assoc_all(mysqli_stmt_get_result($userStatusStatement));
    if (count($userStatusResults) > 0 && count($userStatusResults) < 2) {
        $isAdministrator = $userStatusResults[0]["administrator"];
    }
    mysqli_stmt_close($userStatusStatement);
}

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Discussion Forums | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="../assets/css/main.css"/>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong>Members</strong>Centre</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#">Forums</a></li>
            <?php
            if ($isAdministrator) {
                echo "<li><a href='#'>Admin Panel</a></li>";
            } ?>
            <li><a href="management.php">My Account</a></li>
            <li><a href="?logout">Logout</a></li>
        </ul>
    </nav>
</header>

<a href="#" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">

        <header class="major special">
            <h2>Forum Categories</h2>
            <p>All categories of the discussion forum is listed here</p>
        </header>

        <!-- Categories -->
        <?php
        $categoriesQuery = "SELECT * FROM post_categories ORDER BY categoryID ASC";
        $categoriesResult = mysqli_query($link, $categoriesQuery);
        $categoriesRows = mysqli_fetch_assoc_all($categoriesResult);

        for ($i = 0; $i < count($categoriesRows); $i++) {
            $categoryName = $categoriesRows[$i]['categoryName'];
            $categoryDescription = $categoriesRows[$i]['categoryDescription'];
            $categoryID = urlencode($categoriesRows[$i]['categoryID']);
            echo
        "<section>
            <header>
                <a href=\"forum.php?section=$categoryID\"><h3>$categoryName</h3></a>
                <p>$categoryDescription</p>
            </header>
        </section>";
            if ($i < count($categoriesRows) - 1) {
                // Print horizontal rule
                echo "<hr />";
            }
        }

//        $sectionsResult = mysqli_query($link, "SHOW COLUMNS FROM posts WHERE Field = 'category'");
//        preg_match("/^enum\(\'(.*)\'\)$/", mysqli_fetch_assoc_all($sectionsResult)[0]["Type"], $matches);
//        $sections = explode("','", $matches[1]);
//
//        for ($i = 0; $i < count($sections); $i++) {
//            $sectionNameEncoded = urlencode($sections[$i]);
//            echo
//        "<section>
//            <header>
//                <a href=\"forum.php?section=$sectionNameEncoded\"><h3>$sections[$i]</h3></a>
//            </header>
//        </section>";
//            if ($i < count($sections) - 1) {
//                // Print horizontal rule
//                echo "<hr />";
//            }
//        }
        ?>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/skel.min.js"></script>
<script src="../assets/js/util.js"></script>
<script src="../assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>