<?php

require 'mysqlHelper.php';

require_once __DIR__ .'/libraries/csrf/csrfprotector.php';
csrfProtector::init();

$formSubmissionMessage = ""; // This will be displayed at the end of the form

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Validation of length
    if (strlen($_POST["name"]) > 127) {
        $formSubmissionMessage .= "<p>Name is too long, it has to be limited to 127 characters only.</p>";
    }
    if (strlen($_POST["comments"]) > 1023) {
        $formSubmissionMessage .= "<p>Message is too long, it has to be limited to 1023 characters only.</p>";
    }

    $name = substr(filter_var($_POST["name"], FILTER_SANITIZE_FULL_SPECIAL_CHARS), 0, 127);
    $email = substr(filter_var($_POST["email"], FILTER_VALIDATE_EMAIL), 0, 127);
    $comments = substr(filter_var($_POST["comments"], FILTER_SANITIZE_FULL_SPECIAL_CHARS), 0, 1023);

    // Check email validity
    if (!$email) {
        $formSubmissionMessage .= "<p>Email is not valid, please enter a valid email</p>";
    }

    // Verify captcha
    $post_data = http_build_query(
        array(
            'secret' => '6LdbjCoUAAAAAAreeT9Yy7o_-qKsZodKOhJq6LmL',
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        )
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);
    if (!$result->success) {
        $formSubmissionMessage .= "<p>CAPTCHA failed. Please retry the CAPTCHA challenge</p>";
    }

    // Final check to submit only when all fields are valid
    if (strlen($formSubmissionMessage) < 1) {
        $contactUsFormQuery = "INSERT INTO contact_us (name, email, comments) VALUES (?, ?, ?)";
        $contactUsFormStatement = mysqli_prepare($link, $contactUsFormQuery);
        if ($contactUsFormStatement) {
            // DBMS successfully accepted prepared statement
            mysqli_stmt_bind_param($contactUsFormStatement, "sss", $name, $email, $comments); //this is actually correct, ignore the warning
            mysqli_stmt_execute($contactUsFormStatement);
            $contactUsFormStatus = mysqli_stmt_error($contactUsFormStatement);
            if (strlen($contactUsFormStatus) == 0) {
                // Successfully registered!!!
                $formSubmissionMessage = "<p>Successfully submitted, our people will be in touch with you shortly</p>";
            } else {
                echo "Something wrong";
            }
            mysqli_stmt_close($contactUsFormStatement);
        } else {
            $formSubmissionMessage .= "<p>Failed to submit, error code: FAILED_PREP_SQL</p>";
        }
    }
}

mysqli_close($link);

?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Contact Us | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="assets/css/main.css"/>

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong><a href="index.php">Make</a></strong> by SP</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="index.php#about">About Us</a></li>
            <li><a href="index.php#portfolio">Portfolio</a></li>
            <li><a href="index.php#exco">Our Exco</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="members/index.php">Members' Area</a></li>
        </ul>
    </nav>
</header>

<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">
        <section>
            <h2>Contact Us</h2>
            <p>We are an established CCA in Singapore Polytechnic, 500 Dover Road, Singapore 139651. If you would like to contact us, please fill in this form below</p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="row uniform 50%">
                    <div class="6u 12u$(xsmall)">
                        <input type="text" name="name" id="name" value="" placeholder="Name"/>
                    </div>
                    <div class="6u$ 12u$(xsmall)">
                        <input type="email" name="email" id="email" value="" placeholder="Email"/>
                    </div>
                    <div class="12u$">
                        <textarea name="comments" id="message" placeholder="Enter your message" rows="6"></textarea>
                    </div>
                    <div class="12u$">
                        <div class="g-recaptcha" data-sitekey="6LdbjCoUAAAAAGo3X_UdZKX9OGj19KB6iQd45qI4"></div>
                    </div>
                    <div class="12u$">
                        <ul class="actions">
                            <li><input type="submit" value="Send Message" class="special"/></li>
                            <li><input type="reset" value="Reset"/></li>
                        </ul>
                    </div>
                </div>
            </form>
            <?php echo $formSubmissionMessage; ?>
        </section>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->

</body>
</html>