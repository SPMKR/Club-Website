<?php
require_once __DIR__ .'/libraries/csrf/csrfprotector.php';
csrfProtector::init();
require_once 'mysqlHelper.php';
?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Home | SP Makers' Club</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="landing">

<!-- Header -->
<header id="header" class="alt">
    <h1><strong><a href="index.php">Make</a></strong> by SP</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#about">About Us</a></li>
            <li><a href="#portfolio">Portfolio</a></li>
            <li><a href="#exco">Our Exco</a></li>
            <li><a href="#contactUs">Contact Us</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="members/index.php">Members' Area</a></li>
        </ul>
    </nav>
</header>

<!--  -->
<!--<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>-->
<a class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Banner -->
<section id="banner">
    <h2>SP Maker's Club</h2>
    <p>Make a difference together</p>
    <ul class="actions">
        <li><a href="#about" class="button special big">Get To Know Us</a></li>
    </ul>
</section>

<!-- One -->
<section id="about" class="wrapper style1">
    <div class="container 75%">
        <div class="row 200%">
            <div class="6u 12u$(medium)">
                <header class="major">
                    <h2>Make the future</h2>
                    <p>Make a difference together with us</p>
                </header>
            </div>
            <div class="6u$ 12u$(medium)">
                <p>At Singapore Polytechnic's Makers' Club, we aim to provide students with quality resources and people to bring their ideas from the drawing board to reality.</p>
                <p>SP Makers' Club is a multidisciplinary club consisting of designers, EEEs and mechanical engineers. Together, we provide the resources for you to bring your project to life.</p>
            </div>
        </div>
    </div>
</section>

<!-- Two -->
<section id="portfolio" class="wrapper style2 special">
    <div class="container">
        <header class="major">
            <h2>Our Portfolio</h2>
            <p>What we've done so far</p>
        </header>
        <?php
        $portfolio_result = mysqli_query($link, "SELECT * FROM portfolio_items ORDER BY created ASC");
        $total_count = mysqli_num_rows($portfolio_result);
        if ($total_count > 0) {
            $row_count = ceil($total_count / 2); // Number of rows to generate
            $portfolio_rows = mysqli_fetch_assoc_all($portfolio_result);
            $printed_totals = 0;
            for ($i = 0; $i < $row_count; $i++) {
                echo "<div class=\"row 150%\">";
                // Actually print out content here, since there are two columns
                for ($j = 0; $j < 2; $j++) {
                    echo
            "<div class=\"6u 12u$(xsmall)\">
                <div class=\"image fit captioned\">
                    <img src=\"images/".rawurlencode($portfolio_rows[$printed_totals]['title']).".jpg\" alt=\"\" />
                    <h3>".$portfolio_rows[$printed_totals]['title']."</h3>
                </div>
            </div>";
                    $printed_totals++;
                    if ($printed_totals >= $total_count) {
                        break;
                    }
                }
                echo "</div>";
            }
        }
        // TODO: Might want to add some error message here if there are no entries
        ?>
    </div>
</section>

<!-- Three -->
<section id="exco" class="wrapper style1">
    <div class="container">
        <header class="major special">
            <h2>Meet our Exco</h2>
            <p>President, Vice President, Secretary and Quartermaster</p>
        </header>
        <div class="feature-grid">
            <?php
            $exco_query = "SELECT * FROM exco_members ORDER BY FIELD(role, 'President','Vice President','Secretary','Quartermaster')";
            $exco_result = mysqli_query($link, $exco_query);

            while ($row = mysqli_fetch_assoc($exco_result)) {
                echo
            "<div class=\"feature\">
                <div class=\"image rounded\"><img src=\"images/".rawurlencode($row['name']).".jpg\" alt=\"".$row['name']."\" /></div>
                <div class=\"content\">
                    <header>
                        <h4>".$row['name']."</h4>
                        <p>".$row['role']."</p>
                    </header>
                    <p>".$row['description']."</p>
                </div>
            </div>";
            }

            mysqli_close($link); // This must be done every single time!!
            ?>
        </div>
    </div>
</section>

<!-- Four -->
<section id="contactUs" class="wrapper style3 special">
    <div class="container">
        <header class="major">
            <h2>Contact Us</h2>
            <p>Contact us for learning opportunities, workshops and more</p>
        </header>
        <ul class="actions">
            <li><a href="contact_us.php" class="button special big">Get in touch</a></li>
        </ul>
    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
<!--            <li><a href="#" class="icon fa-twitter"></a></li>-->
<!--            <li><a href="#" class="icon fa-instagram"></a></li>-->
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/easing.min.js"></script>
<script src="assets/js/main.js"></script>
<!--<script src="assets/js/retina.min.js"></script>-->


</body>
</html>
