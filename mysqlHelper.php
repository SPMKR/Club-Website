<?php
/**
 * Created by PhpStorm.
 * User: panziyue
 * Date: 8/8/17
 * Time: 10:40 PM
 *
 * This file takes care of all the MySQL connections in a single file that one can easily "require" in the source code
 */

// Helper Function definitions

// This helper function allows one to rapidly retrieve data from an SQL query
// returning null if there's nothing
// The correct way to get data from this is by performing $array_name[$column_name][$index]
function mysqli_fetch_assoc_all($result) {
    if (!$result) {
        return null;
    }
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
    return $data;
}

// MySQL DB connection

$link = mysqli_connect('127.0.0.1', 'makers', 'TestPassword123', 'makers');

if (!$link) {
    die("
<html>
<head>
    <title>Under Maintenance | SP Makers' Club</title>
    <meta charset=\"utf-8\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
    <link rel=\"stylesheet\" href=\"assets/css/main.css\" />
</head>
<body>
    <div class=\"container\">
        <h2>This website is under maintenance, please check back later</h2>
        <h1>Error code: ".mysqli_connect_error()."</h1>
    </div>
</body>
</html>
");
}
?>
