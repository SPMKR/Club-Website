<?php

require_once "mysqlHelper.php";

require_once __DIR__ .'/libraries/csrf/csrfprotector.php';
csrfProtector::init();

if (isset($authenticated) && $authenticated) {
    header('Location: '.'members/index.php');
    exit();
}

// Define vars and make sure they're empty
$studentID = $password = $passwordAgain = "";
$captchaValid = false; // This variable determines if the CAPTCHA is valid

$isRegistering = false;

// Assume least optimal state (everything is bad)
$passwordMatching = false;
$studentIDFieldValid = false;
$passwordViolations = "<p>Something went wrong with the password validator, please try again later</p>";
$memberExists = false;
$realNameValid = false;
$registerStatus = "";

// Returns empty string if no password violations, otherwise, returns all password violations
function validatePassword($password)
{
    $passwordViolations = "";
    if (strlen($password) < 10) {
        // Password is less than 10 characters
        $passwordViolations .= "<p>Password cannot be shorter than 10 characters</p>";
    }
    if (!preg_match("#[0-9]+#", $password)) {
        $passwordViolations .= "<p>Password must contain at least 1 number</p>";
    }
    if (!preg_match("#[A-Z]+#", $password)) {
        $passwordViolations .= "<p>Password must contain at least 1 upper case letter</p>";
    }
    if (!preg_match("#[a-z]+#", $password)) {
        $passwordViolations .= "<p>Password must contain at least 1 lower case letter</p>";
    }
    return $passwordViolations;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Valid POST command. Attempt to register
    $isRegistering = true;

    # Verify captcha
    $post_data = http_build_query(
        array(
            'secret' => '6LdbjCoUAAAAAAreeT9Yy7o_-qKsZodKOhJq6LmL',
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        )
    );
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $post_data
        )
    );
    $context  = stream_context_create($opts);
    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $result = json_decode($response);
    if ($result->success) { // successful
        $captchaValid = true;
    }

    $studentID = strtolower(filter_var($_POST["studentID"], FILTER_SANITIZE_STRING)); // This one is alphanumeric only
    $password = filter_var($_POST["password"], FILTER_SANITIZE_FULL_SPECIAL_CHARS); // Allow special characters
    $passwordAgain = filter_var($_POST["passwordAgain"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

    // The following code does field validation
    if ($password == $passwordAgain) {
        // Password matching
        $passwordMatching = true;
    }
    $passwordViolations = validatePassword($password);
    if (preg_match("/^p\d{7}$/", $studentID)) {
        $studentIDFieldValid = true;
    }
    // Check if user already exists
    $members_result = mysqli_query($link, "SELECT * FROM members ORDER BY username ASC");
    while ($row = mysqli_fetch_assoc($members_result)) {
        if ($row["username"] == $studentID) {
            $memberExists = true;
        }
    }

    // Resolve username to real name and check if the student ID is valid
    $nameJSONResponseFile = file_get_contents("http://mobileappnew.sp.edu.sg/spexamtt_sas2_server/source/examtt.php?id=" . substr($studentID, 1));
    $nameJSONResponse = json_decode($nameJSONResponseFile, true);
    $realName = $nameJSONResponse["student"][0]["data"]["name"];
    if ($realName != "-") {
        $realNameValid = true;
    }

    // Final test for validity
    if ($passwordMatching &&
        $studentIDFieldValid &&
        strlen($passwordViolations) == 0 &&
        !$memberExists &&
        $realNameValid &&
        $captchaValid) {
        // All the tests pass through, go for registration

        // Hash password
        $passwordHashed = password_hash($password, PASSWORD_DEFAULT);

        // Execute prepared statement
        $registerQuery = "INSERT INTO members (username, password, name) VALUES (?, ?, ?)";
        $registerStatement = mysqli_prepare($link, $registerQuery);
        if ($registerStatement) {
            // DBMS successfully accepted prepared statement
            mysqli_stmt_bind_param($registerStatement, "sss", $studentID, $passwordHashed, $realName); //this is actually correct, ignore the warning
            mysqli_stmt_execute($registerStatement);
            $registerStatus .= mysqli_stmt_error($registerStatement);
            if (strlen($registerStatus) == 0) {
                // Successfully registered!!!
                $registerStatus = "Registration succeeded! Please wait for our administrators to approve you. In the meantime, you can log in";
            }
            mysqli_stmt_close($registerStatement);
        } else {
            $registerStatus = "Registration failed, error code: FAILED_PREP_SQL";
        }
    }
} else {
    // If the code ends up here, the user is simply visiting the page, so...no registering going on here
    $isRegistering = false;
}
mysqli_close($link);
?>
<!DOCTYPE html>
<!--
    Spatial by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Register | SP Makers' Club</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="assets/css/main.css"/>

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<!-- Header -->
<header id="header">
    <h1><strong><a href="index.php">Make</a></strong> by SP</h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="index.php#about">About Us</a></li>
            <li><a href="index.php#portfolio">Portfolio</a></li>
            <li><a href="index.php#exco">Our Exco</a></li>
            <li><a href="index.php#contactUs">Contact Us</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="#">Members' Area</a></li>
        </ul>
    </nav>
</header>

<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

<!-- Main -->
<section id="main" class="wrapper">
    <div class="container">

        <!-- Form -->
        <section>
            <h3>Register</h3>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="row uniform 50%">
                    <div class="6u 12u$(xsmall)">
                        <input type="text" name="studentID" placeholder="Student ID (pXXXXXXX)"/>
                    </div>
                </div>
                <div class="row uniform 50%">
                    <div class="6u 12u$(xsmall)">
                        <input type="password" name="password" placeholder="Password"/>
                    </div>
                </div>
                <div class="row uniform 50%">
                    <div class="6u 12u$(xsmall)">
                        <input type="password" name="passwordAgain" placeholder="Password (again)"/>
                    </div>
                    <div class="12u$">
                        <div class="g-recaptcha" data-sitekey="6LdbjCoUAAAAAGo3X_UdZKX9OGj19KB6iQd45qI4"></div>
                    </div>
                    <div class="12u$">
                        <ul class="actions">
                            <li><input type="submit" value="Register" class="special"/></li>
                            <li><input type="button" onclick="location.href='login.php'" value="Log In"/></li>
                        </ul>
                    </div>
                </div>
            </form>
            <?php
            if ($isRegistering) {
                // Show these messages only if the user is registering
                if (!$captchaValid) {
                    echo "<p>CAPTCHA failed. Please retry the CAPTCHA challenge</p>";
                }
                if ($memberExists) {
                    // This actually means member exists, but we have to prevent user enumeration
                    echo "<p>Successfully registered! Completion code: 0000</p>";
                }
                if (!$studentIDFieldValid) {
                    echo "<p>Student ID is invalid, must be in format pXXXXXXX, where X is a number</p>";
                }
                if (!$passwordMatching) {
                    echo "<p>Passwords do not match!</p>";
                } else {
                    echo $passwordViolations;
                }
                if (strlen($registerStatus) > 0) {
                    echo "<p>$registerStatus</p>";
                }
            }
            ?>
        </section>

    </div>
</section>

<!-- Footer -->
<footer id="footer">
    <div class="container">
        <ul class="icons">
            <li><a href="https://www.facebook.com/Make.Club.SP/" class="icon fa-facebook"></a></li>
            <!--            <li><a href="#" class="icon fa-twitter"></a></li>-->
            <!--            <li><a href="#" class="icon fa-instagram"></a></li>-->
        </ul>
        <ul class="copyright">
            <li>&copy; SPMKR 2017</li>
            <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
            <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
        </ul>
    </div>
</footer>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

</body>
</html>