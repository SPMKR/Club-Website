This text file documents the ideas for the CSAD project for reference

MakerFoundry LLP Corporate Website (old idea)
 - Basic User Interface
 - Good navigation
 - Contact Us page
 - Website Business Info
 - Rating System for products
 - Image Gallery -> Showcasing products/Pictures
 - Project portfolio and project catalog // SPEEEduino, Instructables tutorial, RetroPi, StatiX BASIC
 - Photo files upload -> Administrator to dynamically update company products
 - Internal Quick Search -> Look for company products (clarify with teacher)
 - Member Registration & Login -> Technical support and email subscription management, including admin login
 - Discussion Forum -> Technical support tickets/feedback

SP Maker's Club Website

NOTE: Left side is from marksheet, right side is what we are implementing. The following is copied from the marking sheet

Basic features:
    Basic User Interface Design -> Just a user interface
    Immediate, Intuitive Navigation -> Navigation bar with some dropdown menus
    Contact us with database -> Contact us that automatically goes to database
    Website Business Information -> Information on the club, about, activities, people, etc.
    Image Galleries -> Images of events
    Project Portfolio (About Us) -> Projects we've done
    Flash Banner Animation -> ???
    Consistent, Apparent Branding -> yes, duh
    Search external Website -> ???
    News and Announcements -> Announcements for members
    Product Catalog (with pictures) -> Very similar to project portfolio...maybe they're the same?
    Good program design & data validation -> We'll try our best
    Rating System -> Rating for workshops??

Database design:
    Photos and/or video files upload -> Upload to portfolio or image gallery??
    Quick Search (internal) -> Search for?? Discussion Forum posts??
    CRUD Functions for Website Data -> Already done with a discussion forum
    Member Registration and Login -> Relatively simple. Implement bcrypt based password hashing with salt
    Discussion Forum -> Club member's discussion forum. Not public.

 Template
 We have now used the Bell Template from https://bootstrapmade.com for CSAD project