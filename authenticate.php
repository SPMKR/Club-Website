<?php
/**
 * Created by PhpStorm.
 * User: panziyue
 * Date: 5/8/17
 * Time: 2:17 PM
 *
 * This file is purely PHP code for checking if users are signed in (i.e. is their JWT valid). If invalid, redirect to ../login.php
 *
 */

require_once "libraries/jwt/JWT.php";
require_once "libraries/jwt/BeforeValidException.php";
require_once "libraries/jwt/ExpiredException.php";
require_once "libraries/jwt/SignatureInvalidException.php";
use \Firebase\JWT\JWT;

$token = isset($_COOKIE["authorization"]) ? $_COOKIE["authorization"] : null;

$authenticated = false;

$error = "";

$userID = "";
$issuedAt = "";
$expiresAt = "";

if ($token) {
    // If token even exists
    try {
        $decoded = JWT::decode($token, "X#XGlc\$O]D4n@l)&t-l,Q^>/=%41-\"6Wz`8!.#cpGMjQckl6<\$AE6vJ[8X#G!'7X", array('HS256'));
        $userID = $decoded->aud;
        $issuedAt = $decoded->nbf;
        $expiresAt= $decoded->exp;
        $authenticated = true;
    } catch (\Firebase\JWT\BeforeValidException $e) {
        // The token is not yet valid
        $error = $e;
    } catch (\Firebase\JWT\ExpiredException $e) {
        // The token has expired
        $error = $e;
    } catch (\Firebase\JWT\SignatureInvalidException $e) {
        // Signature is invalid
        $error = $e;
    }
}

if ($error) {
    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    $folder = $protocol . $_SERVER['SERVER_NAME'];
    $path = substr(__FILE__, strlen($_SERVER[ 'DOCUMENT_ROOT']));
    $path = substr($path, 0, strlen($path)-strlen("authenticate.php")); // Get root directory

    $url = $folder . $path;
    // Token is invalid
    header('Location: '.$url.'login.php');
    exit();
}
